<?php

//#Soal String No 1
$panjangString1 = strlen("Hello PHP!");
$jumlahKata1 = str_word_count("Hello PHP!");
$panjangString2 = strlen("I'm ready for the challenges");
$jumlahKata2 = str_word_count("I'm ready for the challenges");

echo "Soal String Nomor 1";
echo "<hr>";
echo "Panjang string kalimat pertama : " . $panjangString1;
echo "<br>";
echo " jumlah kata kalimat pertama : " . $jumlahKata1;
echo "<br>";
echo "<br>";
echo "Panjang string kalimat kedua : " . $panjangString2;
echo "<br>";
echo " jumlah kata kalimat kedua : " . $jumlahKata2;
echo "<br>";
echo "<br>";

//#Soal String No 2
echo "Soal String Nomor 2";
echo "<hr>";
$string2 = "I love PHP"; 
echo "String : " . $string2 . "<br>";
echo "Kata pertama : " . substr($string2, 0, 1) . "<br>" ;
echo "Kata kedua : " . substr($string2, 2, 5) . "<br>";
echo "Kata Ketiga: " . substr($string2, 7, 9) . "<br>" . "<br>";

//#Soal String No 3
echo "Soal String Nomor 3";
echo "<hr>";

$string3 = "PHP is old but sexy!";
echo "String: " . $string3 . "<br>";
echo "Diubah menjadi : " . str_replace("sexy","awesome",$string3) . "<br>" . "<br>";

//#Soal Array No 1
echo "Soal Array Nomor 1";
echo "<hr>";
$kids = [
	"Mike", 
	"Dustin", 
	"Will", 
	"Lucas", 
	"Max", 
	"Eleven"];

$adults = [
	"Hopper", 
	"Nancy",  
	"Joyce", 
	"Jonathan", 
	"Murray"];

print_r($kids);
echo "<br>";
print_r($adults);
echo "<br><br>";

//#Soal Array No 2
echo "Soal Array Nomor 2";
echo "<hr>";
echo "<br>";
echo "Cast Stranger Things: ";
echo "<br>";
echo "Total Kids: " . count($kids);
echo "<br>";
echo "<ol>";
echo "<li> $kids[0] </li> <br>";
echo "<li> $kids[1] </li> <br>";
echo "<li> $kids[2] </li> <br>";
echo "<li> $kids[3] </li> <br>";
echo "<li> $kids[4] </li> <br>";
echo "<li> $kids[5] </li> <br> <br>";

echo "</ol>";
echo "Total Adults: " . count($adults);
echo "<br>";
echo "<ol>";
echo "<li> $adults[0] </li> <br>";
echo "<li> $adults[1] </li> <br>";
echo "<li> $adults[2] </li> <br>";
echo "<li> $adults[3] </li> <br>";
echo "<li> $adults[4] </li> <br>";
    // Lanjutkan

    echo "</ol>";

//Soal Array Nomor 3

/*
            SOAL No 3
            Susun data-data berikut ke dalam bentuk Asosiatif Array didalam Array Multidimensi
            
            Name: "Will Byers"
            Age: 12,
            Aliases: "Will the Wise"
            Status: "Alive"

            Name: "Mike Wheeler"
            Age: 12,
            Aliases: "Dungeon Master"
            Status: "Alive"

            Name: "Jim Hopper"
            Age: 43,
            Aliases: "Chief Hopper"
            Status: "Deceased"

            Name: "Eleven"
            Age: 12,
            Aliases: "El"
            Status: "Alive"
*/

echo "Soal Array Nomor 3";
echo "<hr>";
$person = [    
	0 => ["name"=>"Will Byers","age"=>"12", "aliases"=>"Will the Wise", "status"=>"Alive"],
	1 => ["name"=>"Mike Wheeler","age"=>"12", "aliases"=>"Dungeon Master", "status"=>"Alive"],
	2 => ["name"=>"Jim Hopper","age"=>"43", "aliases"=>"Chief Hopper", "status"=>"Deceased"],
	3 => ["name"=>"Eleven","age"=>"12", "aliases"=>"El", "status"=>"Alive"]
];
print_r($person);


?>